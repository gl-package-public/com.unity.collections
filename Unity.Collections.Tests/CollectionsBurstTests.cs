// Disable burst compatiblity tests, as they are checking invalid cases 
// (EntityManager.AddComponent can be used with managed types for instance)
#if UNITY_EDITOR && false

using NUnit.Framework;
using Unity.Collections.Tests;

[TestFixture, EmbeddedPackageOnlyTest]
public class CollectionsBurstTests : BurstCompatibilityTests
{
    public CollectionsBurstTests()
        : base("Unity.Collections",
            "Packages/com.unity.collections/Unity.Collections.BurstCompatibilityTestCodeGen/_generated_burst_tests.cs",
            "Unity.Collections.BurstCompatibilityTestCodeGen")
    {
    }
}
#endif

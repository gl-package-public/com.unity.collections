#pragma warning disable 0649

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using AOT;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine.Assertions;

namespace Unity.Collections
{
    public static class IIndexableExtensions
    {
        public static void Fill<T,U>(this ref T t, U v) where T : struct, IIndexable<U> where U : unmanaged
        {
            int l = t.Length;
            for(int i = 0; i < l; ++i)
                 t.ElementAt(i) = v;
        } 
    }

    /// <summary>
    ///
    /// </summary>
    public static class AllocatorManager
    {        
        /// <summary>
        ///
        /// </summary>
        /// <param name="handle"></param>
        /// <param name="itemSizeInBytes"></param>
        /// <param name="alignmentInBytes"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        public unsafe static void* Allocate(AllocatorHandle handle, int itemSizeInBytes, int alignmentInBytes, int items = 1)
        {
            Block block = default;
            block.Range.Allocator = handle;
            block.Range.Items = items;
            block.Range.Pointer = IntPtr.Zero;
            block.BytesPerItem = itemSizeInBytes;
            block.Alignment = alignmentInBytes;
            var error = Try(ref block);
            CheckFailedToAllocate(error);
            return (void*)block.Range.Pointer;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handle"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        public unsafe static T* Allocate<T>(AllocatorHandle handle, int items = 1) where T : unmanaged
        {
            return (T*)Allocate(handle, UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), items);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="handle"></param>
        /// <param name="pointer"></param>
        /// <param name="itemSizeInBytes"></param>
        /// <param name="alignmentInBytes"></param>
        /// <param name="items"></param>
        public unsafe static void Free(AllocatorHandle handle, void* pointer, int itemSizeInBytes, int alignmentInBytes,
            int items = 1)
        {
            if (pointer == null)
                return;
            Block block = default;
            block.Range.Allocator = handle;
            block.Range.Items = 0;
            block.Range.Pointer = (IntPtr)pointer;
            block.BytesPerItem = itemSizeInBytes;
            block.Alignment = alignmentInBytes;
            var error = Try(ref block);
            CheckFailedToFree(error);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="handle"></param>
        /// <param name="pointer"></param>
        public unsafe static void Free(AllocatorHandle handle, void* pointer)
        {
            Free(handle, pointer, 1, 1, 1);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handle"></param>
        /// <param name="pointer"></param>
        /// <param name="items"></param>
        public unsafe static void Free<T>(AllocatorHandle handle, T* pointer, int items = 1) where T : unmanaged
        {
            Free(handle, pointer, UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), items);
        }

        /// <summary>
        /// Corresponds to Allocator.Invalid.
        /// </summary>
        public static readonly AllocatorHandle Invalid = new AllocatorHandle { Index = 0 };

        /// <summary>
        /// Corresponds to Allocator.None.
        /// </summary>
        public static readonly AllocatorHandle None = new AllocatorHandle { Index = 1 };

        /// <summary>
        /// Corresponds to Allocator.Temp.
        /// </summary>
        public static readonly AllocatorHandle Temp = new AllocatorHandle { Index = 2 };

        /// <summary>
        /// Corresponds to Allocator.TempJob.
        /// </summary>
        public static readonly AllocatorHandle TempJob = new AllocatorHandle { Index = 3 };

        /// <summary>
        /// Corresponds to Allocator.Persistent.
        /// </summary>
        public static readonly AllocatorHandle Persistent = new AllocatorHandle { Index = 4 };

        /// <summary>
        /// Corresponds to Allocator.AudioKernel.
        /// </summary>
        public static readonly AllocatorHandle AudioKernel = new AllocatorHandle { Index = 5 };

        /// <summary>
        /// Delegate used for calling an allocator's allocation function.
        /// </summary>
        public delegate int TryFunction(IntPtr allocatorState, ref Block block);

        /// <summary>
        /// Which allocator a Block's Range allocates from.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct AllocatorHandle
        {
            /// <summary>
            ///
            /// </summary>
            /// <param name="a"></param>
            /// <returns></returns>
            public static implicit operator AllocatorHandle(Allocator a) => new AllocatorHandle { Index = (ushort)a };

            /// <summary>
            /// Index into a function table of allocation functions.
            /// </summary>
            public ushort Index;
            public ushort Version;
            
            public int Value => Index;

            /// <summary>
            /// Allocates a Block of memory from this allocator with requested number of items of a given type.
            /// </summary>
            /// <typeparam name="T">Type of item to allocate.</typeparam>
            /// <param name="block">Block of memory to allocate within.</param>
            /// <param name="Items">Number of items to allocate.</param>
            /// <returns>Error code from the given Block's allocate function.</returns>
            public int TryAllocate<T>(out Block block, int Items) where T : struct
            {
                block = new Block
                {
                    Range = new Range { Items = Items, Allocator = this },
                    BytesPerItem = UnsafeUtility.SizeOf<T>(),
                    Alignment = 1 << math.min(3, math.tzcnt(UnsafeUtility.SizeOf<T>()))
                };
                var returnCode = Try(ref block);
                return returnCode;
            }

            /// <summary>
            /// Allocates a Block of memory from this allocator with requested number of items of a given type.
            /// </summary>
            /// <typeparam name="T">Type of item to allocate.</typeparam>
            /// <param name="Items">Number of items to allocate.</param>
            /// <returns>A Block of memory.</returns>
            public Block Allocate<T>(int Items) where T : struct
            {
                var error = TryAllocate<T>(out Block block, Items);
                CheckAllocatedSuccessfully(error);
                return block;
            }

            [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
            static void CheckAllocatedSuccessfully(int error)
            {
                if (error != 0)
                    throw new ArgumentException($"Error {error}: Failed to Allocate");
            }
        }

        /// <summary>
        ///
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct BlockHandle
        {
            /// <summary>
            ///
            /// </summary>
            public ushort Value;
        }

        /// <summary>
        /// Pointer for the beginning of a block of memory, number of items in it, which allocator it belongs to, and which block this is.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct Range : IDisposable
        {
            /// <summary>
            ///
            /// </summary>
            public IntPtr Pointer; //  0

            /// <summary>
            ///
            /// </summary>
            public int Items; //  8

            /// <summary>
            ///
            /// </summary>
            public AllocatorHandle Allocator; // 12

            /// <summary>
            ///
            /// </summary>
            public void Dispose()
            {
                Block block = new Block { Range = this };
                block.Dispose();
                this = block.Range;
            }
        }

        /// <summary>
        /// A block of memory with a Range and metadata for size in bytes of each item in the block, number of allocated items, and alignment.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct Block : IDisposable
        {
            /// <summary>
            ///
            /// </summary>
            public Range Range;

            /// <summary>
            /// Number of bytes in each item requested.
            /// </summary>
            public int BytesPerItem;

            /// <summary>
            /// How many items were actually allocated.
            /// </summary>
            public int AllocatedItems;

            /// <summary>
            /// (1 &lt;&lt; this) is the byte alignment.
            /// </summary>
            public byte Log2Alignment;

            /// <summary>
            ///
            /// </summary>
            public byte Padding0;

            /// <summary>
            ///
            /// </summary>
            public ushort Padding1;

            /// <summary>
            ///
            /// </summary>
            public uint Padding2;

            /// <summary>
            ///
            /// </summary>
            public long Bytes => BytesPerItem * Range.Items;

            /// <summary>
            ///
            /// </summary>
            public int Alignment
            {
                get => 1 << Log2Alignment;
                set => Log2Alignment = (byte)(32 - math.lzcnt(math.max(1, value) - 1));
            }

            /// <summary>
            ///
            /// </summary>
            public void Dispose()
            {
                TryFree();
            }

            /// <summary>
            ///
            /// </summary>
            /// <returns></returns>
            public int TryAllocate()
            {
                Range.Pointer = IntPtr.Zero;
                return Try(ref this);
            }

            /// <summary>
            ///
            /// </summary>
            /// <returns></returns>
            public int TryFree()
            {
                Range.Items = 0;
                return Try(ref this);
            }

            /// <summary>
            ///
            /// </summary>
            public void Allocate()
            {
                var error = TryAllocate();
                CheckFailedToAllocate(error);
            }

            /// <summary>
            ///
            /// </summary>
            public void Free()
            {
                var error = TryFree();
                CheckFailedToFree(error);
            }

            [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
            void CheckFailedToAllocate(int error)
            {
                if (error != 0)
                    throw new ArgumentException($"Error {error}: Failed to Allocate {this}");
            }

            [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
            void CheckFailedToFree(int error)
            {
                if (error != 0)
                    throw new ArgumentException($"Error {error}: Failed to Free {this}");
            }
        }

        /// <summary>
        /// An allocator with a tryable allocate/free/realloc function pointer.
        /// </summary>
        public interface IAllocator
        {
            /// <summary>
            ///
            /// </summary>
            TryFunction Function { get; }

            /// <summary>
            ///
            /// </summary>
            /// <param name="block"></param>
            /// <returns></returns>
            int Try(ref Block block);

            /// <summary>
            /// Upper limit on how many bytes this allocator is allowed to allocate.
            /// </summary>
            long BudgetInBytes { get; }

            /// <summary>
            /// Number of currently allocated bytes for this allocator.
            /// </summary>
            long AllocatedBytes { get; }
            
            AllocatorHandle Parent { get; }            
        }        

        internal static Allocator LegacyOf(AllocatorHandle handle)
        {
            if (handle.Value >= FirstUserIndex)
                return Allocator.Persistent;
            return (Allocator) handle.Value;
        }

        static unsafe int TryLegacy(ref Block block)
        {
            if (block.Range.Pointer == IntPtr.Zero) // Allocate
            {
                block.Range.Pointer = (IntPtr)Memory.Unmanaged.Allocate(block.Bytes, block.Alignment, LegacyOf(block.Range.Allocator));
                block.AllocatedItems = block.Range.Items;
                return (block.Range.Pointer == IntPtr.Zero) ? -1 : 0;
            }
            if (block.Bytes == 0) // Free
            {
                Memory.Unmanaged.Free((void*) block.Range.Pointer, LegacyOf(block.Range.Allocator));
                block.Range.Pointer = IntPtr.Zero;
                block.AllocatedItems = 0;
                return 0;
            }
            // Reallocate (keep existing pointer and change size if possible. otherwise, allocate new thing and copy)
            return -1;
        }

        /// <summary>
        /// Looks up an allocator's allocate, free, or realloc function pointer from a table and invokes the function.
        /// </summary>
        /// <param name="block">Block to allocate memory for.</param>
        /// <returns>Error code of invoked function.</returns>
        public static unsafe int Try(ref Block block)
        {
            if (block.Range.Allocator.Value < FirstUserIndex)
                return TryLegacy(ref block);
            TableEntry tableEntry = default;
            tableEntry = SharedStatics.TableEntry.Ref.Data.ElementAt(block.Range.Allocator.Value);
            var function = new FunctionPointer<TryFunction>(tableEntry.function);
            // if the allocator being passed in has a version of 0, that means "whatever the current version is."
            // so we patch it here, with whatever the current version is...
            if(block.Range.Allocator.Version == 0)
                block.Range.Allocator.Version = SharedStatics.Version.Ref.Data.ElementAt(block.Range.Allocator.Value);
            // this is really bad in non-Burst C#, it generates garbage each time we call Invoke
            return function.Invoke(tableEntry.state, ref block);
        }

        /// <summary>
        /// Stack allocator with no backing storage.
        /// </summary>
        [BurstCompile(CompileSynchronously = true)]
        internal struct StackAllocator : IAllocator, IDisposable
        {
            public AllocatorHandle Parent => m_storage.Range.Allocator;
            
            internal Block m_storage;
            internal long m_top;

            internal long budgetInBytes;
            public long BudgetInBytes => budgetInBytes;

            internal long allocatedBytes;
            public long AllocatedBytes => allocatedBytes;

            public unsafe int Try(ref Block block)
            {
                if (block.Range.Pointer == IntPtr.Zero) // Allocate
                {
                    if (m_top + block.Bytes > m_storage.Bytes)
                    {
                        return -1;
                    }

                    block.Range.Pointer = (IntPtr)((byte*)m_storage.Range.Pointer + m_top);
                    block.AllocatedItems = block.Range.Items;
                    allocatedBytes += block.Bytes;
                    m_top += block.Bytes;
                    return 0;
                }

                if (block.Bytes == 0) // Free
                {
                    if ((byte*)block.Range.Pointer - (byte*)m_storage.Range.Pointer == (long)(m_top - block.Bytes))
                    {
                        m_top -= block.Bytes;
                        var blockSizeInBytes = block.AllocatedItems * block.BytesPerItem;
                        allocatedBytes -= blockSizeInBytes;
                        block.Range.Pointer = IntPtr.Zero;
                        block.AllocatedItems = 0;
                        return 0;
                    }

                    return -1;
                }

                // Reallocate (keep existing pointer and change size if possible. otherwise, allocate new thing and copy)
                return -1;
            }

            [BurstCompile(CompileSynchronously = true)]
			[MonoPInvokeCallback(typeof(TryFunction))]
            public static unsafe int Try(IntPtr allocatorState, ref Block block)
            {
                return ((StackAllocator*)allocatorState)->Try(ref block);
            }

            public TryFunction Function => Try;

            public void Dispose()
            {
            }
        }

        /// <summary>
        /// Slab allocator with no backing storage.
        /// </summary>
        [BurstCompile(CompileSynchronously = true)]
        internal struct SlabAllocator : IAllocator, IDisposable
        {
            public AllocatorHandle Parent => Storage.Range.Allocator;
        
            internal Block Storage;
            internal int Log2SlabSizeInBytes;
            internal FixedListInt4096 Occupied;
            internal long budgetInBytes;
            internal long allocatedBytes;

            public long BudgetInBytes => budgetInBytes;

            public long AllocatedBytes => allocatedBytes;

            internal int SlabSizeInBytes
            {
                get => 1 << Log2SlabSizeInBytes;
                set => Log2SlabSizeInBytes = (byte)(32 - math.lzcnt(math.max(1, value) - 1));
            }

            internal int Slabs => (int)(Storage.Bytes >> Log2SlabSizeInBytes);

            internal SlabAllocator(Block storage, int slabSizeInBytes, long budget)
            {
                Assert.IsTrue((slabSizeInBytes & (slabSizeInBytes - 1)) == 0);
                Storage = storage;
                Log2SlabSizeInBytes = 0;
                Occupied = default;
                budgetInBytes = budget;
                allocatedBytes = 0;
                SlabSizeInBytes = slabSizeInBytes;
                Occupied.Length = (Slabs + 31) / 32;
            }

            public int Try(ref Block block)
            {
                if (block.Range.Pointer == IntPtr.Zero) // Allocate
                {
                    if (block.Bytes + allocatedBytes > budgetInBytes)
                        return -2; //over allocator budget
                    if (block.Bytes > SlabSizeInBytes)
                        return -1;
                    for (var wordIndex = 0; wordIndex < Occupied.Length; ++wordIndex)
                    {
                        var word = Occupied[wordIndex];
                        if (word == -1)
                            continue;
                        for (var bitIndex = 0; bitIndex < 32; ++bitIndex)
                            if ((word & (1 << bitIndex)) == 0)
                            {
                                Occupied[wordIndex] |= 1 << bitIndex;
                                block.Range.Pointer = Storage.Range.Pointer +
                                    (int)(SlabSizeInBytes * (wordIndex * 32U + bitIndex));
                                block.AllocatedItems = SlabSizeInBytes / block.BytesPerItem;
                                allocatedBytes += block.Bytes;
                                return 0;
                            }
                    }

                    return -1;
                }

                if (block.Bytes == 0) // Free
                {
                    var slabIndex = ((ulong)block.Range.Pointer - (ulong)Storage.Range.Pointer) >>
                        Log2SlabSizeInBytes;
                    int wordIndex = (int)(slabIndex >> 5);
                    int bitIndex = (int)(slabIndex & 31);
                    Occupied[wordIndex] &= ~(1 << bitIndex);
                    block.Range.Pointer = IntPtr.Zero;
                    var blockSizeInBytes = block.AllocatedItems * block.BytesPerItem;
                    allocatedBytes -= blockSizeInBytes;
                    block.AllocatedItems = 0;
                    return 0;
                }

                // Reallocate (keep existing pointer and change size if possible. otherwise, allocate new thing and copy)
                return -1;
            }

            [BurstCompile(CompileSynchronously = true)]
			[MonoPInvokeCallback(typeof(TryFunction))]
            public static unsafe int Try(IntPtr allocatorState, ref Block block)
            {
                return ((SlabAllocator*)allocatorState)->Try(ref block);
            }

            public TryFunction Function => Try;

            public void Dispose()
            {
            }
        }

        /// <summary>
        /// Mapping between a Block, AllocatorHandle, and an IAllocator.
        /// </summary>
        /// <typeparam name="T">Type of allocator to install functions for.</typeparam>
        public struct AllocatorInstallation<T> : IDisposable
            where T : unmanaged, IAllocator, IDisposable
        {
            /// <summary>
            ///
            /// </summary>
            public Block MBlock;

            /// <summary>
            ///
            /// </summary>
            public AllocatorHandle m_handle;

            unsafe T* t => (T*)MBlock.Range.Pointer;

            /// <summary>
            ///
            /// </summary>
            public ref T Allocator
            {
                get
                {
                    unsafe
                    {
                        return ref UnsafeUtility.AsRef<T>(t);
                    }
                }
            }

            /// <summary>
            /// Creates a Block for an allocator, associates that allocator with an AllocatorHandle, then installs the allocator's function into the function table.
            /// </summary>
            /// <param name="Handle">Index into function table at which to install this allocator's function pointer.</param>
            public AllocatorInstallation(AllocatorHandle Handle)
            {
                // Allocate an allocator of type T using UnsafeUtility.Malloc with Allocator.Persistent.
                MBlock = Persistent.Allocate<T>(1);
                m_handle = Handle;
                unsafe
                {
                    UnsafeUtility.MemSet(t, 0, UnsafeUtility.SizeOf<T>());
                    Install(m_handle, (IntPtr)t, t->Function);
                }
            }

            /// <summary>
            /// Installs an allocator with the given Creates a Block for an allocator, associates that allocator with an AllocatorHandle, then installs the allocator's function into the function table.
            /// </summary>
            /// <param name="Handle">Index into function table at which to install this allocator's function pointer.</param>
            public AllocatorInstallation(FunctionPointer<TryFunction> functionPointer)
            {
                // Allocate an allocator of type T using UnsafeUtility.Malloc with Allocator.Persistent.
                MBlock = Persistent.Allocate<T>(1);
                m_handle = default;
                unsafe
                {
                    UnsafeUtility.MemSet(t, 0, UnsafeUtility.SizeOf<T>());
                    m_handle = Register((IntPtr)t, functionPointer);
                }
            }

            /// <summary>
            /// Installs an allocator with the given Creates a Block for an allocator, associates that allocator with an AllocatorHandle, then installs the allocator's function into the function table.
            /// </summary>
            /// <param name="Handle">Index into function table at which to install this allocator's function pointer.</param>
            public static AllocatorInstallation<T> New()
            {
                T t = default;
                return new AllocatorInstallation<T>(BurstCompiler.CompileFunctionPointer(t.Function));
            }

            /// <summary>
            ///
            /// </summary>
            public void Dispose()
            {
                Unregister(m_handle);
                unsafe
                {
                    t->Dispose();
                }

                MBlock.Dispose();
            }
        }

        internal struct TableEntry
        {
            internal IntPtr function;
            internal IntPtr state;
        }

        internal struct TableEntry16
        {
            internal TableEntry f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct TableEntry256
        {
            internal TableEntry16 f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct TableEntry4096
        {
            internal TableEntry256 f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct TableEntry32768 : IIndexable<TableEntry>
        {
            internal TableEntry4096 f0, f1, f2, f3, f4, f5, f6, f7;
            public int Length { get { return 32768; } set {} }
            public ref TableEntry ElementAt(int index)
            {
                unsafe { fixed(TableEntry4096* p = &f0) { return ref UnsafeUtility.AsRef<TableEntry>((TableEntry*)p + index); } }
            }
        }
        
        unsafe struct MemoryRange<T> : IIndexable<T> where T : unmanaged
        {
            public T* begin;
            public ulong count;
            public int Length { get { return (int)count; } set { count = (ulong)value; } }
            public ref T ElementAt(int index)
            {
                return ref begin[index];
            }
        }
                
        internal struct UShort16
        {
            internal ushort f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct UShort256
        {
            internal UShort16 f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct UShort4096
        {
            internal UShort256 f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct UShort32768 : IIndexable<ushort>
        {
            internal UShort4096 f0, f1, f2, f3, f4, f5, f6, f7;
            public int Length { get { return 32768; } set {} }
            public ref ushort ElementAt(int index)
            {
                unsafe { fixed(UShort4096* p = &f0) { return ref UnsafeUtility.AsRef<ushort>((ushort*)p + index); } }
            }
        }
        
        internal struct SpinLock16
        {
            internal SpinLock f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct SpinLock256
        {
            internal SpinLock16 f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct SpinLock4096
        {
            internal SpinLock256 f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }

        internal struct SpinLock32768 : IIndexable<SpinLock>
        {
            internal SpinLock4096 f0, f1, f2, f3, f4, f5, f6, f7;
            public int Length { get { return 32768; } set {} }
            public ref SpinLock ElementAt(int index)
            {
                unsafe { fixed(SpinLock4096* p = &f0) { return ref UnsafeUtility.AsRef<SpinLock>((SpinLock*)p + index); } }
            }
        }
        
        public struct SpinLock
        {
            internal int value;
            internal void Lock()
            {
                while(Interlocked.CompareExchange(ref value, 1, 0) != 0)
                {
                }
            }
            internal void Unlock()
            {
                while(Interlocked.CompareExchange(ref value, 0, 1) != 1)
                {
                } 
            }
        }

#if ENABLE_UNITY_COLLECTIONS_CHECKS

        /// <summary>
        ///   <para>Determines if the handle is still valid, because we intend to release it if it is.</para>
        /// </summary>
        /// <param name="handle">Safety handle.</param>
        internal static unsafe bool CheckExists(AtomicSafetyHandle handle)
        {
#if UNITY_DOTSRUNTIME
            int* versionNode = (int*) (void*) handle.nodePtr;
#else        
            int* versionNode = (int*) (void*) handle.versionNode;
#endif            
            return handle.version == (*versionNode & -8);
        }
        
        internal static unsafe bool AreTheSame(AtomicSafetyHandle a, AtomicSafetyHandle b)
        {
            if(a.version != b.version)
                return false;
#if UNITY_DOTSRUNTIME
            if(a.nodePtr != b.nodePtr)
#else
            if(a.versionNode != b.versionNode)
#endif
                return false;
            return true;
        }

        internal struct ChildHandles 
        {
            internal UnsafeList childSafetyHandles;
        }
        internal struct ChildHandlesAccessor : IDisposable
        {
            int index;
            ref ChildHandles reference { get {
                return ref DependentTable.Ref.Data.ElementAt(index); 
            } }
            ref SpinLock SpinLock { get {
                return ref SharedStatics.SpinLock.Ref.Data.ElementAt(index); 
            } }
            internal ChildHandlesAccessor(int i)
            {
                index = i;
                SpinLock.Lock();
            }
            internal bool NeedsUseAfterFreeTracking()
            {            
                if(index < FirstUserIndex)
                    return false;
                if(reference.childSafetyHandles.Allocator.Value != (int)Allocator.Persistent)
                    return false; 
                return true;
            }
            public const int InvalidSafetyHandleIndex = -1;
            internal int AddSafetyHandle(AtomicSafetyHandle handle)
            {
                if(!NeedsUseAfterFreeTracking())
                    return InvalidSafetyHandleIndex;
                var result = reference.childSafetyHandles.Length;
                reference.childSafetyHandles.Add(handle);
                return result;
            }
            internal bool TryRemoveSafetyHandle(AtomicSafetyHandle handle, int safetyHandleIndex)
            {
                if(!NeedsUseAfterFreeTracking())
                    return false;
                if(safetyHandleIndex == InvalidSafetyHandleIndex)
                    return false;
                safetyHandleIndex = math.min(safetyHandleIndex, reference.childSafetyHandles.Length - 1);
                while(safetyHandleIndex >= 0)
                {
                    unsafe
                    {
                        var safetyHandle = (AtomicSafetyHandle*)reference.childSafetyHandles.Ptr + safetyHandleIndex;
                        if(AreTheSame(*safetyHandle, handle))
                        {
                            reference.childSafetyHandles.RemoveAtSwapBack<AtomicSafetyHandle>(safetyHandleIndex);
                            return true;
                        }
                    }
                    --safetyHandleIndex;
                }
                return false;
            }
            internal void ReleaseSafetyHandles()
            {
                if(!NeedsUseAfterFreeTracking())
                    return;
                for(var i = 0; i < reference.childSafetyHandles.Length; ++i)
                {
                    unsafe
                    {
                        AtomicSafetyHandle* handle = (AtomicSafetyHandle*)reference.childSafetyHandles.Ptr + i;
                        if(CheckExists(*handle))
                            AtomicSafetyHandle.Release(*handle);
                    }
                }
                reference.childSafetyHandles.Clear();
            }
            public void Dispose()
            {
                SpinLock.Unlock();
            }
        }
        struct ChildHandles16
        {
            internal ChildHandles f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }
        struct ChildHandles256
        {
            internal ChildHandles16 f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }
        struct ChildHandles4096
        {
            internal ChildHandles256 f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15;
        }
        struct ChildHandles32768 : IIndexable<ChildHandles>
        {
            internal ChildHandles4096 f0, f1, f2, f3, f4, f5, f6, f7;
            public int Length { get { return 32768; } set {} }
            public ref ChildHandles ElementAt(int index)
            {
                unsafe { fixed(ChildHandles4096* p = &f0) { return ref UnsafeUtility.AsRef<ChildHandles>((ChildHandles*)p + index); } }
            }
        }
        
        sealed class DependentTable
        {
            public static readonly SharedStatic<ChildHandles32768> Ref =
                SharedStatic<ChildHandles32768>.GetOrCreate<DependentTable>();
        }

#endif

        /// <summary>
        /// SharedStatic that holds array of allocation function pointers for each allocator.
        /// </summary>
        internal sealed class SharedStatics
        {
            internal sealed class IsInstalled { internal static readonly SharedStatic<Long1024> Ref = SharedStatic<Long1024>.GetOrCreate<IsInstalled>(); }
            internal sealed class TableEntry { internal static readonly SharedStatic<TableEntry32768> Ref = SharedStatic<TableEntry32768>.GetOrCreate<TableEntry>(); }
            internal sealed class Version { internal static readonly SharedStatic<UShort32768> Ref = SharedStatic<UShort32768>.GetOrCreate<Version>(); }
            internal sealed class SpinLock { internal static readonly SharedStatic<SpinLock32768> Ref = SharedStatic<SpinLock32768>.GetOrCreate<SpinLock>(); }
        }

        /// <summary>
        /// Initializes SharedStatic allocator function table and allocator table, and installs default allocators.
        /// </summary>
        public static void Initialize()
        {
            unsafe 
            {
                SharedStatics.Version.Ref.Data.Fill(default(ushort));
                SharedStatics.IsInstalled.Ref.Data.Fill(default(long));
                SharedStatics.IsInstalled.Ref.Data.ElementAt(0) = ~0L;
#if ENABLE_UNITY_COLLECTIONS_CHECKS                
                fixed (ChildHandles32768* dependency = &DependentTable.Ref.Data)
                {
                    for(var i = 0; i < dependency->Length; ++i)
                    {
                        dependency->ElementAt(i).childSafetyHandles = new UnsafeList(Allocator.Persistent);
                    }
                }
#endif                
            }
        }

        /// <summary>
        /// Creates and saves allocators' function pointers into function table.
        /// </summary>
        /// <param name="handle">AllocatorHandle to allocator to install function for.</param>
        /// <param name="allocatorState">IntPtr to allocator's custom state.</param>
        /// <param name="functionPointer">Function pointer to create or save in function table.</param>
        public static unsafe void Install(AllocatorHandle handle, IntPtr allocatorState, FunctionPointer<TryFunction> functionPointer)
        {
            var tableEntry = new TableEntry { state = allocatorState, function = functionPointer.Value };
            int error;
            if(functionPointer.Value == IntPtr.Zero)
                error = ConcurrentMask.TryFree(ref SharedStatics.IsInstalled.Ref.Data, handle.Value);
            else
                error = ConcurrentMask.TryAllocate(ref SharedStatics.IsInstalled.Ref.Data, handle.Value); 
            if(ConcurrentMask.Succeeded(error))
            {
                ++SharedStatics.Version.Ref.Data.ElementAt(handle.Value);
                SharedStatics.TableEntry.Ref.Data.ElementAt(handle.Value) = tableEntry;
            }
        }

        /// <summary>
        /// Creates and saves allocators' function pointers into function table.
        /// </summary>
        /// <param name="handle">AllocatorHandle to allocator to install function for.</param>
        /// <param name="allocatorState">IntPtr to allocator's custom state.</param>
        /// <param name="function">Function pointer to create or save in function table.</param>
        public static void Install(AllocatorHandle handle, IntPtr allocatorState, TryFunction function)
        {
            var functionPointer = (function == null)
                ? new FunctionPointer<TryFunction>(IntPtr.Zero)
                : BurstCompiler.CompileFunctionPointer(function);
            Install(handle, allocatorState, functionPointer);
        }

        /// <summary>
        /// Threadsafely finds a slot for an allocator that isn't currently in use, and installs the
        /// user-provided function and state pointers into that slot.
        /// </summary>
        /// <param name="allocatorState">IntPtr to allocator's custom state.</param>
        /// <param name="function">Function pointer to create or save in function table.</param>
        /// <returns>An AllocatorHandle that refers to the newly-allocated slot.</returns>
        public static unsafe AllocatorHandle Register(IntPtr allocatorState, FunctionPointer<TryFunction> functionPointer)
        {
            var tableEntry = new TableEntry { state = allocatorState, function = functionPointer.Value };
            var error = ConcurrentMask.TryAllocate(ref SharedStatics.IsInstalled.Ref.Data, out int offset);
            AllocatorHandle handle = default;
            if(ConcurrentMask.Succeeded(error))
            {
                handle.Index = (ushort)offset;
                handle.Version = ++SharedStatics.Version.Ref.Data.ElementAt(handle.Value);
                SharedStatics.TableEntry.Ref.Data.ElementAt(handle.Value) = tableEntry;
            }                    
            return handle;
        }

        public static unsafe AllocatorHandle Register<T>(ref T t) where T : unmanaged, IAllocator
        {
            var functionPointer = (t.Function == null)
                ? new FunctionPointer<TryFunction>(IntPtr.Zero)
                : BurstCompiler.CompileFunctionPointer(t.Function);
            return Register((IntPtr)UnsafeUtility.AddressOf(ref t), functionPointer);
        }
            
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            
        /// <summary>
        /// We're about to clear an Allocator - to release the memory it has claimed.
        /// So first we must release the Safety Nodes and Allocators that were carved from this Allocator.
        /// This is to prevent any possible use-after-free bugs.
        /// </summary>
        /// <param name="handle">The AllocatorHandle whose Child Handles will be Released.</param>
        public static void ReleaseChildHandles(AllocatorHandle handle)
        {
            using(var accessor = new ChildHandlesAccessor(handle.Value))
                accessor.ReleaseSafetyHandles();
        }

#endif
            
        /// <summary>
        /// Threadsafely removes an allocator from the global table.
        /// </summary>
        /// <param name="handle">The AllocatorHandle to uninstall.</param>
        public static unsafe void Unregister(AllocatorHandle handle)
        {
            var error = ConcurrentMask.TryFree(ref SharedStatics.IsInstalled.Ref.Data, handle.Value);
            if(ConcurrentMask.Succeeded(error))
            {
#if ENABLE_UNITY_COLLECTIONS_CHECKS                
                ReleaseChildHandles(handle);
#endif
                SharedStatics.TableEntry.Ref.Data.ElementAt(handle.Value) = default;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public static void Shutdown()
        {
        }

        /// <summary>
        /// User-defined allocator index.
        /// </summary>
        public const ushort FirstUserIndex = 64;

        [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
        internal static void CheckFailedToAllocate(int error)
        {
            if (error != 0)
                throw new ArgumentException("failed to allocate");
        }

        [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
        internal static void CheckFailedToFree(int error)
        {
            if (error != 0)
                throw new ArgumentException("failed to free");
        }
    }
}

#pragma warning restore 0649
